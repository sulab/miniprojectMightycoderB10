<?php

    include_once ('phonebook/PhoneBook.php');
    use Phone_book\phonebook\PhoneBook;
        
    $user = new PhoneBook();
    $user1 = $user->show($_GET['id']);
       
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="icon" href="images/phonebook.ico">
        <link rel="stylesheet" href="bootstrap/bootstrap.min.css" >
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="custom_style.css">
	<title>Add Number</title>
</head>

<body>
	<div class="container">
		<br>
		<div class="header clearfix">
        <nav class="navbar navbar-default" role="navigation">
        	<!-- Brand and toggle get grouped for better mobile display -->
        	<div class="navbar-header">
        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        			<span class="sr-only">Toggle navigation</span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        		</button>
        		<a class="navbar-brand" href="index.php">Mini Project</a>
        	</div>
        
        	<!-- Collect the nav links, forms, and other content for toggling -->
        	<div class="collapse navbar-collapse navbar-ex1-collapse">
        		<!-- <form class="navbar-form navbar-left" role="search">
        			<div class="form-group">
        				<input type="text" class="form-control" placeholder="Search Phone Number">
        			</div>
        			<button type="submit" class="btn btn-primary">Search</button>
        		</form> -->
        		<ul class="nav navbar-nav navbar-right">
        			<li ><a href="index.php">Phonebook</a></li>
        			<li><a href="add.php">Add Number</a></li>      			
        		</ul>
        	</div><!-- /.navbar-collapse -->
        </nav>
        </div>

		<div class="jumbotron">
			<div class="container">
				<form action="update.php" method="post" role="form">
					<legend>Edit Your Number</legend>
                                        
                                        <div class="form-group">
						
                                            <input type="hidden" class="form-control" name="id" value="<?php echo $user1['id'];?>">
					</div>
					<div class="form-group">
						<label for="lname">User Name</label>
                                                <input type="text" class="form-control" name="username" value="<?php echo $user1['username'];?>" id="lname" placeholder="Enter First Name" selected>
					</div>
					<div class="form-group">
						<label for="Phone">Phone Number</label>
                                                <input type="tel"  class="form-control" name="phonenumber" value="<?php echo $user1['phonenumber'];?>" id="Phone" placeholder="Enter First Name">
					</div>
					<div class="form-group">
						<label for="text">Address</label>
                                                <input type="text" class="form-control" name="address" value="<?php echo $user1['address'];?>" id="email" placeholder="Enter First Email">
					</div>
                                        <input type="submit" class="btn btn-primary" value="Edit">

				</form>
			</div>
		</div>

<!-- 		<div class="row">
			<div class="col-md-1 offset"></div>
			<div class="col-md-10">
				
			</div>
				
			<div class="col-md-1 offset"></div>		
		</div> -->
		<div class="footer">
 			<hr>
 			 <p>Copyright&copy; Miniproject 2016</p>
 		</div>

	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
        <script src="bootstrap/bootstrap.min.js" ></script>	
</body>

</html>