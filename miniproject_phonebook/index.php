<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="icon" href="images/phonebook.ico">
        <link rel="stylesheet" href="bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="custom_style.css">
          <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 
	<title>Phone Book</title>
</head>

<body>
	<div class="container"><br>
            <?php
                include_once 'phonebook/PhoneBook.php';
                use Phone_book\phonebook\PhoneBook;
                
                $user = new PhoneBook();
                $name = $user->getName();
                
                $search = array();
                
                if($_SERVER['REQUEST_METHOD'] == 'POST'){
                    $search = $_POST;
                    $users = $user->index($search);
                }else {
                    $users = $user->index();   
                }
                $search = isset($_POST['search'])?$_POST['search']:"";
                
                
                
                
                //var_dump($users);
                //die()
            ?>
		<div class="header clearfix">
	        <nav class="navbar navbar-default" role="navigation">
	        	<!-- Brand and toggle get grouped for better mobile display -->
	        	<div class="navbar-header">
	        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	        			<span class="sr-only">Toggle navigation</span>
	        			<span class="icon-bar"></span>
	        			<span class="icon-bar"></span>
	        			<span class="icon-bar"></span>
	        		</button>
	        		<a class="navbar-brand" href="#">Mini Project</a>
	        	</div>	        
	        	<!-- Collect the nav links, forms, and other content for toggling -->
	        	<div class="collapse navbar-collapse navbar-ex1-collapse">
	        		<form action="index.php" method="post" class="navbar-form navbar-left pull-left" role="search">
	        			<div class="form-group">
                                            <input type="text" name="search" class="form-control" id="tags" placeholder="Search By Name" value="<?php echo $search;?>">
	        			</div>
	        			<a href="search.php?username=<?php echo $users['username'];?>"><button type="submit" class="btn btn-primary">Search</button></a>
	        		</form>
	        		<ul class="nav navbar-nav navbar-right">
                                        <li><a href="pdf.php">Download As PDF</a></li>
                                        <li><a href="01simple-download-xlsx.php">Download As Xl</a></li>
	        			<li><a href="create.php">Add Number</a></li>        			
	        		</ul>
	        	</div><!-- /.navbar-collapse -->	        	
	        </nav>
        </div>


		<div class="jumbotron">
			<div class="container">
				<table class="table table-condensed table-striped table-hover">
					<thead>
						<tr>
							<th>
								<i class="fa fa-user"></i>&nbsp;Name
							</th>
							<th>
								<i class="fa fa-mobile"></i>&nbsp;Phone Number
							</th>
							<th>
								<i class="fa fa-envelope"></i>&nbsp;Address
							</th>
							<th>
								<i class="fa fa-cogs"></i>&nbsp;Options
							</th>
						</tr>						
					</thead>

					<tbody>
                                            
            <?php
               foreach($users as $user1){ //$dates catch the DB's data and $bdate is an local variable
            ?>
                                            
						<tr>
							<td><?php echo $user1['username'];?></td>
							<td><?php echo "0".$user1['phonenumber'];?></td>
							<td><?php echo $user1['address'];?></td>
							<td>
								<a class="btn btn-success" href="view.php?id=<?php echo $user1['id'];?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>&nbsp;view</button></a>&nbsp;
								<a class="btn btn-primary" href="edit.php?id=<?php echo $user1['id'];?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>&nbsp;edit</button></a>&nbsp;
								<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal<?php echo $user1['id'];?>"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>&nbsp;delete</button>
                                                    
             
                       
                                                                <!-- Modal -->
								<div class="modal fade" id="myModal<?php echo $user1['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                                    <div class="modal-dialog" role="document" id="myModal<?php echo $user1['id'];?>">
								    <div class="modal-content" >
								      <div class="modal-header" >
								        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								      </div>
								      <div class="modal-body text-center" >
                                                                          <p><strong>Are you sure you want to delete ?</strong></p>
                                                                           
                                                                          <a href="delete.php?id=<?php echo $user1['id'];?>" class="btn btn-default" >Confirm</a>
                                                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                                     
								      </div>
								    </div>
								  </div>
								</div><!-- /.modal -->

                    					</td>
						</tr>
           <?php
               }
           ?>
					</tbody>

				</table>
				<ul class="pagination pagination-lg text-center">
					<li><a href="#">&laquo;</a></li>
					<li class="active"><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">&raquo;</a></li>
				</ul>
			</div>
		</div>

		<!-- <div class="row">
			<div class="col-md-1 offset"></div>
			<div class="col-md-10">
				
			</div>
				
			<div class="col-md-1 offset"></div>		
		</div>
 -->
 		<div class="footer">
                    <hr>
                    <p>Copyright&copy; Miniproject 2016</p>
                </div>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
          
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
        <script src="bootstrap/bootstrap.min.js" ></script>
         <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
  $(function() {
    var availableTags = [
      '<?php echo implode("','", $name);?>'
    ];
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  });
  </script>
</body>

</html>