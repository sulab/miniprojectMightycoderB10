<?php
ini_set('display_errors','off');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mini Project Registration</title>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-3dkvEK0WLHRJ7/Csr0BZjAWxERc5WH7bdeUya2aXxdU= sha512-+L4yy6FRcDGbXJ9mPG8MT/3UCDzwR9gPeyFNMCtInsol++5m3bk2bXWKdZjvybmohrAsn3Ua5x8gfLnbE1YkOg==" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="custom.css">
    </head>
    <body>

        <div class="container-fluid">

            <?php
            include_once ('registration/Registration.php');

            use user_registration\registration\Registration;

          $user = new Registration();
            $users = $user->index();
            ?>

            

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.php">View registered user</a></li>
                        <li><a href="create.php">Registration</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            
        </div>

        <div class="container-fluid header">
            <div class="row">
                <div class="col-md-offset-3 col-md-6 text-center">
                    <h1>View Registered User</h1>
                </div>
            </div>	
        </div>


                <div class="row">
                    <div class="col-md-offset-1 col-md-10">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center"><i class="fa fa-user"></i>&nbsp;First Name</th>
                                    <th class="text-center"><i class="fa fa-user"></i>&nbsp;Last Name</th>
                                    <th class="text-center"><i class="fa fa-user"></i>&nbsp;Sex</th>
                                    <th class="text-center"><i class="fa fa-user"></i>&nbsp;City</th>
                                    <th class="text-center"><i class="fa fa-user"></i>&nbsp;Hobby</th>
                                    <th class="text-center"><i class="fa fa-building-o"></i>&nbsp;Organization</th>
                                    <th class="text-center"><i class="fa fa-envelope-o"></i>&nbsp;E-mail</th>
                                    <th class="text-center"><i class="fa fa-info-circle"></i>&nbsp;personal Details</th>
                                    <th class="text-center"><i class="fa fa-cogs"></i>&nbsp;Settings</th>
                                </tr>				
                            </thead>

                            <tbody>

                                <?php
                                foreach ($users as $user1) {
                                    ?>

                                    <tr class="active">
                                        <td><?php echo $user1['fname'] ?></td>
                                        <td><?php echo $user1['lname'] ?></td>
                                        <td><?php echo $user1['sex'] ?></td>
                                        <td><?php echo $user1['city'] ?></td>
                                        <td><?php echo $user1['hobby'] ?></td>
                                        <td><?php echo $user1['ORGname'] ?></td>
                                        <td><?php echo $user1['email'] ?></td>
                                        <td id="info"><?php echo $user1['PersonalInfo'] ?></td>
                                        
                                        <td><a class="btn btn-success" href="view.php?id=<?php echo $user1['id']; ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>&nbsp; View</a></td>
                                            <td><a class="btn btn-info" href="edit.php?id=<?php echo $user1['id']; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>&nbsp; Edit</a></td>
                                            <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal<?php echo $user1['id']; ?>">&nbsp;<span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>&nbsp; Delete</button></td>

                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal<?php echo $user1['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document" id="myModal<?php echo $user1['id']; ?>">
                                                    <div class="modal-content">
                                                        
                                                        <div class="modal-body text-center">
                                                            <p><strong>Are you sure you want to delete?</strong></p>

                                                            <a href="delete.php?id=<?php echo $user1['id']; ?>" class="btn btn-default" >Confirm</a>
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.modal -->

                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>

                        </table>
                    </div>
                </div>	


        <div class="container-fluid footer text-center" style="color:white;">
            <p>Copyright &copy;</p>
        </div>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==" crossorigin="anonymous"></script>
    </body>
</html>