<?php
ini_set('display_errors', 'off');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mini Project Registration</title>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-3dkvEK0WLHRJ7/Csr0BZjAWxERc5WH7bdeUya2aXxdU= sha512-+L4yy6FRcDGbXJ9mPG8MT/3UCDzwR9gPeyFNMCtInsol++5m3bk2bXWKdZjvybmohrAsn3Ua5x8gfLnbE1YkOg==" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="custom.css">
        <style>
            table{
                width: 100%
            }
        </style>
    </head>
    <body>

        <div class="container-fluid">

            <?php
            include_once ('registration/Registration.php');

            use user_registration\registration\Registration;

            $user = new Registration();
            $users = $user->show($_GET['id']);
            ?>

            <nav class="navbar navbar-default" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Mini Project #2</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.php">View registered user</a></li>
                        <li><a href="create.php">Registration</a></li>
                        <li><a href="pdf.php?id=<?php echo $users['id']?>">Download as PDF</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
        </div>

        <div class="container-fluid header">
            <div class="row">
                <div class="col-md-offset-3 col-md-6 text-center">
                    <h1>View Registered User</h1>
                </div>
            </div>	
        </div>



        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <dl style="color: #ffffff;">
                    <dt><i class="fa fa-user"></i>&nbsp;First Name</dt>
                    <dd><?php echo $users['fname'] ?></dd><br>

                    <dt ><i class="fa fa-user"></i>&nbsp;Last Name</dt>
                    <dd><?php echo $users['lname'] ?></dd><br>

                    <dt ><i class="fa fa-user"></i>&nbsp;Sex</dt>
                    <dd><?php echo $users['sex'] ?></dd><br>

                    <dt ><i class="fa fa-user"></i>&nbsp;City</dt>
                    <dd><?php echo $users['city'] ?></dd><br>

                    <dt ><i class="fa fa-user"></i>&nbsp;Hobby</dt>
                    <dd><?php echo $users['hobby'] ?></dd><br>

                    <dt ><i class="fa fa-building-o"></i>&nbsp;Organization</dd>
                    <dd><?php echo $users['ORGname'] ?></dd><br>

                    <dt ><i class="fa fa-envelope-o"></i>&nbsp;E-mail</dt>
                    <dd><?php echo $users['email'] ?></dd><br>

                    <dt ><i class="fa fa-info-circle"></i>&nbsp;personal Details</dt>
                    <dd id="info"><?php echo $users['PersonalInfo'] ?></dd>

                </dl>
            </div>
        </div>	



        <div class="container-fluid footer text-center" style="color:white;">
            <p>Copyright &copy;</p>
        </div>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==" crossorigin="anonymous"></script>
    </body>
</html>