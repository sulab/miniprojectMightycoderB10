-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2016 at 11:18 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `userregistration`
--

-- --------------------------------------------------------

--
-- Table structure for table `phonenumber`
--

CREATE TABLE IF NOT EXISTS `phonenumber` (
`id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `phonenumber` int(11) NOT NULL,
  `address` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phonenumber`
--

INSERT INTO `phonenumber` (`id`, `username`, `phonenumber`, `address`) VALUES
(15, 'dsfsfd', 2147483647, 'fsfsdfsdfsd'),
(18, 'sulab', 125478522, 'sdada');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `hobby` varchar(255) NOT NULL,
  `ORGname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `PersonalInfo` text NOT NULL,
  `terms` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fname`, `lname`, `sex`, `city`, `hobby`, `ORGname`, `email`, `password`, `PersonalInfo`, `terms`) VALUES
(1, 'Sulab', 'Saha', '', '', '', 'BITM', 'sulab_novel@yahoo.com', '44283562', '<p>sfsfsfsfsf</p>', 'agreed'),
(2, 'pial', 'noman', 'male', 'rajsahi', 'Reading Book,Traveling', 'basis', 'a@B.COM', '1234', '<p>sdftger zsdfh nb zdfh zdfh zdfhb</p>', 'agreed'),
(3, 'gfhhy', 'ghfjncfgsh', 'male', 'chittagong', 'Reading Book', 'dxtyijsfxh', 'xy@zfth.gfhsfgh', 'zhxfgh', '<p>zdfhzfgh xsh rh z hsfgh</p>', 'agreed'),
(4, '', '', '', 'Select', '', '', '', '', '', ''),
(5, '', '', '', '', '', '', 'a@b.com', '1234', '', ''),
(6, '', '', '', '', '', '', 'abc@d.com', '1234', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `phonenumber`
--
ALTER TABLE `phonenumber`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `phonenumber`
--
ALTER TABLE `phonenumber`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
