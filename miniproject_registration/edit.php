    
<?php
include_once ('registration/Registration.php');
ini_set('display_errors','off');
use user_registration\registration\Registration;

$user = new Registration();
$users = $user->show($_GET['id']);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mini Project Registration</title>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-3dkvEK0WLHRJ7/Csr0BZjAWxERc5WH7bdeUya2aXxdU= sha512-+L4yy6FRcDGbXJ9mPG8MT/3UCDzwR9gPeyFNMCtInsol++5m3bk2bXWKdZjvybmohrAsn3Ua5x8gfLnbE1YkOg==" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="custom.css">
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            height: 200,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste imagetools"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });
    </script>
    </head>
    <body>

        <div class="container-fluid">
            <nav class="navbar navbar-default" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Mini Project #2</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.php">View registered user</a></li>
                        <li><a href="create.php">Registration</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
        </div>

        <div class="container-fluid header">
            <div class="row">
                <div class="col-md-offset-3 col-md-6 text-center">
                    <h1>Edit User Info</h1>
                </div>
            </div>	
        </div>

        <div class="jumbotron">
            <div class="container">
                <div class="row">
                            <div class="col-xs-offset-1 col-xs-10 col-sm-offset-2 col-sm-8 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
                        <form role="form" action="update.php" method="POST">
                            
                       <div class="form-group">
                            <input type="hidden" class="form-control" name="id" value="<?php echo $users['id']; ?>">
                        </div>
                            
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-fw fa-user"></i>
                                    </div>
                                    <input class="form-control input-lg" placeholder="First Name"  type="text" name="fname" value="<?php echo $users['fname'];?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-fw fa-user"></i>
                                    </div>
                                    <input class="form-control input-lg" placeholder="Last Name"  type="text" name="lname" value="<?php echo $users['lname'];?>"></div>
                            </div>
                            
                    <label style="color: white;">Select Gender:</label>
                    <div class="form-group">
                        <div class="radio radio-inline">
                          <label>
                            <input type="radio" name="sex" id="optionsRadios1" value="male">
                            Male
                          </label>
                        
                          <label>
                            <input type="radio" name="sex" id="optionsRadios2" value="female">
                            Female
                          </label>
                        
                          <label>
                            <input type="radio" name="sex" id="optionsRadios3" value="other">
                            Other
                          </label>
                        </div>
                    </div>
                            
                    <label style="color: white;">Select Your Hobby:</label>
                    <div class="form-group"  style="color: white;">
                        <div class="checkbox-inline">
                            <label class="checkbox-inline">
                              <input type="checkbox" name="hobby[]" id="inlineCheckbox1" value="Reading Book">Reading Book
                            </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="hobby[]" id="inlineCheckbox2" value="Traveling">Traveling
                            </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" name="hobby[]" id="inlineCheckbox3" value="Driving">Driving
                            </label>
                        </div>
                    </div>
                            
                    <label style="color: white;">Select City:</label>
                    <div class="form-group">
                        <select class="form-control" name="city">
                        <option >Select</option>
                          <option value="dhaka">Dhaka</option>
                          <option value="chittagong">Chittagong</option>
                          <option value="sylhet">Sylhet</option>
                          <option value="khulna">Khulna</option>
                          <option value="rajsahi">Rajsahi</option>
                          <option value="borishal">Borishal</option>
                        </select>
                    </div>

                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-fw fa-group"></i>
                                    </div>
                                    <input class="form-control input-lg " placeholder="Organization Name"  type="text" name="ORGname" value="<?php echo $users['ORGname'];?>">
                                </div>
                            </div>

                            <div class="form-group" >
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-fw fa-envelope-o"></i>
                                    </div>
                                    <input class="form-control input-lg " ng-model="form.email" placeholder="E-mail"  type="text" name="email" value="<?php echo $users['email'];?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-fw fa-key"></i>
                                    </div>
                                    <input class="form-control input-lg" placeholder="Choose Password"  type="password" name="password" value="<?php echo $users['password'];?>">
                                </div>
                            </div>
                    
                      <label style="color: white;">Write something about yourself:</label>
                      <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-keyboard-o"></i>
                            </div>
                                                    
                            <textarea name="PersonalInfo" id="input" class="form-control" rows="3" placeholder="Enter Your Details" data-toggle="tooltip" data-placement="right" title="Choose Your Details"><?php echo $users['PersonalInfo'];?></textarea>
                        </div>
                    </div>
                            
					<div class="form-group"><p class="checkbox text-center">
						<label>
						<input class="" style="vertical-align: middle" id="agree"  type="checkbox" name="terms" value="agreed"> I Agree to the 
						<a class="text-success" href="#" target="_blank" tabindex="8">Terms of Service</a> 
					</div>
                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-lg btn-block" >Edit & Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>	
            </div>
        </div>
        
        <div class="container-fluid footer text-center" style="color:white;">
            <p>Copyright &copy;</p>
        </div>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==" crossorigin="anonymous"></script>
    </body>
</html>