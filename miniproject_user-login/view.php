<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Mini Project Registration</title>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-3dkvEK0WLHRJ7/Csr0BZjAWxERc5WH7bdeUya2aXxdU= sha512-+L4yy6FRcDGbXJ9mPG8MT/3UCDzwR9gPeyFNMCtInsol++5m3bk2bXWKdZjvybmohrAsn3Ua5x8gfLnbE1YkOg==" crossorigin="anonymous">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="custom.css">
</head>
<body>

<div class="container-fluid">
    
    <?php
    include_once ('registration/Registration.php');
    use user_registration\registration\Registration;
    
    $user=new Registration();
    $users=$user->show($_GET['id']);
    ?>
    
	<nav class="navbar navbar-default" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">Mini Project #2</a>
		</div>
	
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav navbar-right">
                            <li><a href="user_list.php">View registered user</a></li>
				<li><a href="create.php">Registration</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</nav>
</div>

<div class="container-fluid header">
	<div class="row">
		<div class="col-md-offset-3 col-md-6 text-center">
			<h1>View Registered User</h1>
		</div>
	</div>	
</div>

<div class="jumbotron">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-1 col-md-10">
				<table class="table table-hover">
					<thead>
						<tr>
							<th class="text-center"><i class="fa fa-user"></i>&nbsp;First Name</th>
							<th class="text-center"><i class="fa fa-user"></i>&nbsp;Last Name</th>
							<th class="text-center"><i class="fa fa-building-o"></i>&nbsp;Organization Name</th>
							<th class="text-center"><i class="fa fa-envelope-o"></i>&nbsp;E-mail</th>
							<th class="text-center"><i class="fa fa-cogs"></i>&nbsp;Settings</th>
						</tr>				
					</thead>
					
					<tbody>
						<tr class="active">
							<td><?php echo $users['fname'];?></td>
							<td><?php echo $users['lname'];?></td>
							<td><?php echo $users['ORGname'];?></td>
							<td><?php echo $users['email'];?></td>
							<td>
								<a class="btn btn-info" href="edit.php?id=<?php echo $users['id'];?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>&nbsp; Edit</a>
								
                                                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">&nbsp;<span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>&nbsp; Delete</button>
                                    <!-- Modal -->
                                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <p><strong>Are you sure you want to delete?</strong></p>

                                                        <a href="delete.php?id=<?php echo $users['id']; ?>" class="btn btn-default" >Confirm</a>
                                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /.modal -->
							</td>
						</tr>
					</tbody>

				</table>
			</div>
		</div>	
	</div>
</div>

<div class="container-fluid footer">
	<p>Copyright &copy;</p>
</div>
	

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==" crossorigin="anonymous"></script>
</body>
</html>